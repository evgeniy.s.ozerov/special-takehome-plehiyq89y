
class ItemsTaxCalculator
  EXEMPT_CATEGOIRIES = %w[
      books
      food
      medical
  ].freeze

  # population should be extended separately
  PRODUCTS = {
      books: %w[book books],
      food: %w[chocolates chocolate],
      medical: %w[headache pills]
  }

  IMPORT_TAX_RATE = 1.05
  TAX_RATE = 1.1

  class << self
    attr_accessor :items, :sales_taxes, :total, :result

    def call(items)
      @items = items
      @sales_taxes = 0.00
      @total = 0.00
      generate_lines
    end

    def generate_lines
      result = items.map(&method(:generate_item))
      result << "Sales Taxes: #{'%.2f' % sales_taxes}" << "Total: #{'%.2f' % total}"
    end

    def generate_item(item)
      initial_price = item.split.last.to_f
      current_price = initial_price.to_f
      quantity = item.split.first.to_i
      current_price = current_price * IMPORT_TAX_RATE if item.split.include?('imported')
      exempt = PRODUCTS.select { |key, value| (item.split & value).any? }.any?
      current_price = current_price.to_f * TAX_RATE if exempt == false
      current_price = ('%.2f' % (current_price.to_f)).to_f
      new_item_line = item.split[0..-2]
      item_total_price = current_price.to_f * quantity
      @total = (@total || 0) + item_total_price
      item_total_taxes = (current_price - initial_price) * quantity
      @sales_taxes = (@sales_taxes || 0) + item_total_taxes.to_f
      new_item_line << item_total_price
      new_item_line.join(' ').gsub(' at', ':')
    end
  end
end

