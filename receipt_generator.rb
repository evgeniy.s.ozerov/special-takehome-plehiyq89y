require_relative 'items_tax_calculator'
class ReceiptGenerator
  ALLOWED_FORMAT = /at\s+[0-9]*\.[0-9]+/
  attr_accessor :input

  def initialize(input)
    @input = Array(input).reject { |element| element == '' }
  end

  def call
    return 'You must provide a non empty input' if input.empty?

    return 'Incorrect input supplied.' unless validate_items

    generate_receipt_items
  end

  private

  # Input validation
  #
  # TODO: move to separate service
  def validate_items
    input.all?(&method(:valid_product_input))
  end

  def valid_product_input(product)
    return false unless product.match?(ALLOWED_FORMAT)

    return false unless valid_quantity?(product)

    true
  end

  def valid_quantity?(product)
    product.split(' ').first.scan(/\D/).empty?
  end

  # Receipt lines generation
  def generate_receipt_items
    ItemsTaxCalculator.call(input)
  end


end