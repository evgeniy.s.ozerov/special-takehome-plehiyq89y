require 'spec_helper'
require_relative '../receipt_generator'
# require_relative '../receipt_items_generator'
RSpec.describe 'ReceiptGenerator' do

  describe 'ReceiptGenerator#call' do
    subject { ReceiptGenerator.new(input).call }
    
    context 'when input is empty' do
      let(:input) { '' }

      it 'should response with an empty input error' do
        expect(subject).to eq('You must provide a non empty input')
      end
    end

    context 'when input is empty array' do
      let(:input) { [''] }

      it 'should response with an empty input error' do
        expect(subject).to eq('You must provide a non empty input')
      end
    end

    context 'when input is invalid' do
      context 'when input has no price' do
        let(:input) { '2 book at ' }

        it 'should response with an empty input error' do
          expect(subject).to eq('Incorrect input supplied.')
        end
      end

      context 'when input has no quantity' do
        let(:input) { 'book at 12.99' }

        it 'should response with an empty input error' do
          expect(subject).to eq('Incorrect input supplied.')
        end
      end
    end

    context 'when input is a valid string' do
      let(:input) { '2 book at 12.49' }
      let(:compact_input) { Array(input)}

      it 'should call ReceiptItemsGenerator with correct arguments' do
        expect(ItemsTaxCalculator).to receive(:call).with(compact_input)
        subject
      end
    end

    context 'when input is an array of valid strings' do
      context 'provided Input 1' do
        let(:input) { [
            '2 book at 12.49',
            '1 music CD at 14.99',
            '1 chocolate bar at 0.85'
        ]
        }
        let(:expected_output) {
          [
            '2 book: 24.98',
            '1 music CD: 16.49',
            '1 chocolate bar: 0.85',
            'Sales Taxes: 1.50',
            'Total: 42.32'
          ]
        }

        it 'should call ReceiptItemsGenerator with correct arguments' do
          expect(subject).to eq(expected_output)
        end
      end

      context 'provided Input 1' do
        let(:input) { [
            '1 imported bottle of perfume at 27.99',
        '1 bottle of perfume at 18.99',
        '1 packet of headache pills at 9.75',
        '3 imported boxes of chocolates at 11.25',
        ]
        }

        # expected output in description is incorrect for line with wine
        # should be 32.33 not 32.19
        let(:expected_output) {
          [
            '1 imported bottle of perfume: 32.33',
            '1 bottle of perfume: 20.89',
            '1 packet of headache pills: 9.75',
            '3 imported boxes of chocolates: 35.55',
            'Sales Taxes: 7.90',
            'Total: 98.38',
          ]
        }

        it 'should call ReceiptItemsGenerator with correct arguments' do
          expect(subject).to eq(expected_output)
        end
      end
    end


  end


end