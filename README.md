# Install
```
gem install rspec
gem install rspec-expectations
```

# Run test
```
rspec spec/receipt_with_taxes_spec.rb
```